package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
    "flag"
    "log"
)


func parseCommandline() (*string, *string) {
    listenAddress := flag.String("listenAddress", "127.0.0.1:2112", "address to listen for requests in ip:port format")
    metricsPath := flag.String("metricsPath", "/safenet", "URL-path where metrics are exported")
    flag.Parse()

    return listenAddress, metricsPath
}




func main() {
    listenAddress, metricsPath := parseCommandline()

	statuspageCollector := newStatuspageCollector()
	prometheus.MustRegister(statuspageCollector)
	http.Handle(*metricsPath, promhttp.Handler())
    log.Printf("starting exporter... at %s, exporting metrics to %s", *listenAddress, *metricsPath)
    http.ListenAndServe(*listenAddress, nil)
    log.Print("exporter running...")

}
