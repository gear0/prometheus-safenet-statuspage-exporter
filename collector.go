package main

import (
	"encoding/json"
	"github.com/prometheus/client_golang/prometheus"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"
)

const (
	URL          = "https://status.safenetid.com/api/v2/components.json"
	US_ZONE_NAME = "US Service Zone"
	EU_ZONE_NAME = "EU Service Zone"
)

type StatusPageResponse struct {
	Page       Page         `json:"page"`
	Components []Components `json:"components"`
}

type Page struct {
	ID        string    `json:"id"`
	Name      string    `json:"name"`
	URL       string    `json:"url"`
	TimeZone  string    `json:"time_zone"`
	UpdatedAt time.Time `json:"updated_at"`
}

type Components struct {
	ID                 string      `json:"id"`
	Name               string      `json:"name"`
	Status             string      `json:"status"`
	CreatedAt          time.Time   `json:"created_at"`
	UpdatedAt          time.Time   `json:"updated_at"`
	Position           int         `json:"position"`
	Description        interface{} `json:"description"`
	Showcase           bool        `json:"showcase"`
	GroupID            string      `json:"group_id"`
	PageID             string      `json:"page_id"`
	Group              bool        `json:"group"`
	OnlyShowIfDegraded bool        `json:"only_show_if_degraded"`
	Components         []string    `json:"components,omitempty"`
}

type ServiceZone struct {
	Name       string
	Components []Components
}

// sets unavailable state in case of communication errors
func (collector *statuspageCollector) setUnavailable(ch chan<- prometheus.Metric) {
	ch <- prometheus.MustNewConstMetric(collector.statusEuAuthSvc, prometheus.GaugeValue, 5)
	ch <- prometheus.MustNewConstMetric(collector.statusEuPushDelivery, prometheus.GaugeValue, 5)
	ch <- prometheus.MustNewConstMetric(collector.statusEuEndUserServices, prometheus.GaugeValue, 5)
	ch <- prometheus.MustNewConstMetric(collector.statusEuAdminConsoles, prometheus.GaugeValue, 5)
	ch <- prometheus.MustNewConstMetric(collector.statusEuMgmt, prometheus.GaugeValue, 5)
	ch <- prometheus.MustNewConstMetric(collector.statusUsAuthSvc, prometheus.GaugeValue, 5)
	ch <- prometheus.MustNewConstMetric(collector.statusUsPushDelivery, prometheus.GaugeValue, 5)
	ch <- prometheus.MustNewConstMetric(collector.statusUsEndUserServices, prometheus.GaugeValue, 5)
	ch <- prometheus.MustNewConstMetric(collector.statusUsAdminConsoles, prometheus.GaugeValue, 5)
	ch <- prometheus.MustNewConstMetric(collector.statusUsMgmt, prometheus.GaugeValue, 5)
}

func (collector *statuspageCollector) doRequest(ch chan<- prometheus.Metric) []byte {
	// query statuspage api
	resp, err := http.Get(URL)

	if err != nil {
		collector.setUnavailable(ch)
		log.Printf("HTTPS request faild with error\n%s", err.Error())
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		collector.setUnavailable(ch)
		log.Printf("Reading request body failed with error\n%s", err.Error())
	}
	return body
}

// get service status for Service Zones
func (collector *statuspageCollector) getServiceStatus(ch chan<- prometheus.Metric) []ServiceZone {
	var euComponentIds []string
	var usComponentIds []string
	var euComponents []Components
	var usComponents []Components
	var serviceZones []ServiceZone

	textBytes := collector.doRequest(ch)
	statusPageResponse := StatusPageResponse{}
	err := json.Unmarshal(textBytes, &statusPageResponse)
	if err != nil {
		collector.setUnavailable(ch)
		log.Printf("Unmarshalling response failed with error\n%s", err.Error())
	}

	// get Service IDs from US and EU Zone Services
	for _, component := range statusPageResponse.Components {
		if component.Name == EU_ZONE_NAME {
			euComponentIds = component.Components
		} else if component.Name == US_ZONE_NAME {
			usComponentIds = component.Components
		}
	}

	for _, componentId := range euComponentIds {
		for _, component := range statusPageResponse.Components {
			if componentId == component.ID {
				euComponents = append(euComponents, component)
				break
			}

		}
	}
	serviceZones = append(serviceZones, ServiceZone{Components: euComponents, Name: EU_ZONE_NAME})

	for _, componentId := range usComponentIds {
		for _, component := range statusPageResponse.Components {
			if componentId == component.ID {
				usComponents = append(usComponents, component)
				break
			}

		}
	}

	serviceZones = append(serviceZones, ServiceZone{Components: usComponents, Name: US_ZONE_NAME})
	log.Print(serviceZones)
	return serviceZones
}

//Define a struct for you collector that contains pointers
//to prometheus descriptors for each metric you wish to expose.
//Note you can also include fields of other types if they provide utility
type statuspageCollector struct {
	statusEuAdminConsoles   *prometheus.Desc
	statusEuPushDelivery    *prometheus.Desc
	statusEuAuthSvc         *prometheus.Desc
	statusEuEndUserServices *prometheus.Desc
	statusEuMgmt            *prometheus.Desc
	statusUsAdminConsoles   *prometheus.Desc
	statusUsPushDelivery    *prometheus.Desc
	statusUsAuthSvc         *prometheus.Desc
	statusUsEndUserServices *prometheus.Desc
	statusUsMgmt            *prometheus.Desc
	mutex                   sync.Mutex
}

//You must create a constructor for your collector that
//initializes every descriptor and returns a pointer to the collector
func newStatuspageCollector() *statuspageCollector {
	return &statuspageCollector{
		statusEuAdminConsoles: prometheus.NewDesc("safenet_statuspageio_statusEuAdminConsoles",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
		statusEuPushDelivery: prometheus.NewDesc("safenet_statuspageio_statusEuPushDelivery",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
		statusEuAuthSvc: prometheus.NewDesc("safenet_statuspageio_statusEuAuthSvc",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
		statusEuEndUserServices: prometheus.NewDesc("safenet_statuspageio_statusEuEndUserServices",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
		statusEuMgmt: prometheus.NewDesc("safenet_statuspageio_statusEuMgmt",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
		statusUsAdminConsoles: prometheus.NewDesc("safenet_statuspageio_statusUsAdminConsoles",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
		statusUsPushDelivery: prometheus.NewDesc("safenet_statuspageio_statusUsPushDelivery",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
		statusUsAuthSvc: prometheus.NewDesc("safenet_statuspageio_statusUsAuthSvc",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
		statusUsEndUserServices: prometheus.NewDesc("safenet_statuspageio_statusUsEndUserServices",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
		statusUsMgmt: prometheus.NewDesc("safenet_statuspageio_statusUsMgmt",
			"0 -> major_outage; 1 -> operational; 2 -> degraded_performance, 3 -> partial_outage; 4 -> maintenanc; 5 service_unavailable",
			nil, nil),
	}
}

//Each and every collector must implement the Describe function.
//It essentially writes all descriptors to the prometheus desc channel.
func (collector *statuspageCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- collector.statusEuAdminConsoles
	ch <- collector.statusEuPushDelivery
	ch <- collector.statusEuAuthSvc
	ch <- collector.statusEuEndUserServices
	ch <- collector.statusEuMgmt
	ch <- collector.statusUsAdminConsoles
	ch <- collector.statusUsPushDelivery
	ch <- collector.statusUsAuthSvc
	ch <- collector.statusUsEndUserServices
	ch <- collector.statusUsMgmt
}

//Collect implements required collect function for all promehteus collectors
func (collector *statuspageCollector) Collect(ch chan<- prometheus.Metric) {
	collector.mutex.Lock()
	defer collector.mutex.Unlock()

	serviceZones := collector.getServiceStatus(ch)
	for _, serviceZone := range serviceZones {
		if serviceZone.Name == EU_ZONE_NAME {
			for _, component := range serviceZone.Components {
				switch component.Name {
				case "Authentication Services":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAuthSvc, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAuthSvc, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAuthSvc, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAuthSvc, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAuthSvc, prometheus.GaugeValue, 4)
					}
				case "Push Delivery":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuPushDelivery, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusEuPushDelivery, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuPushDelivery, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuPushDelivery, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuPushDelivery, prometheus.GaugeValue, 4)
					}
				case "End User Services":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuEndUserServices, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusEuEndUserServices, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuEndUserServices, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuEndUserServices, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuEndUserServices, prometheus.GaugeValue, 4)
					}
				case "Admin Consoles":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAdminConsoles, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAdminConsoles, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAdminConsoles, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAdminConsoles, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuAdminConsoles, prometheus.GaugeValue, 4)
					}
				case "Management":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuMgmt, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusEuMgmt, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuMgmt, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusEuMgmt, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusEuMgmt, prometheus.GaugeValue, 4)
					}
				}
			}
		} else if serviceZone.Name == US_ZONE_NAME {
			for _, component := range serviceZone.Components {
				switch component.Name {
				case "Authentication Services":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAuthSvc, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAuthSvc, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAuthSvc, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAuthSvc, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAuthSvc, prometheus.GaugeValue, 4)
					}
				case "Push Delivery":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsPushDelivery, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusUsPushDelivery, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsPushDelivery, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsPushDelivery, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsPushDelivery, prometheus.GaugeValue, 4)
					}
				case "End User Services":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsEndUserServices, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusUsEndUserServices, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsEndUserServices, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsEndUserServices, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsEndUserServices, prometheus.GaugeValue, 4)
					}
				case "Admin Consoles":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAdminConsoles, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAdminConsoles, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAdminConsoles, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAdminConsoles, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsAdminConsoles, prometheus.GaugeValue, 4)
					}
				case "Management":
					switch component.Status {
					case "major_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsMgmt, prometheus.GaugeValue, 0)
					case "operational":
						ch <- prometheus.MustNewConstMetric(collector.statusUsMgmt, prometheus.GaugeValue, 1)
					case "degraded_performance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsMgmt, prometheus.GaugeValue, 2)
					case "partial_outage":
						ch <- prometheus.MustNewConstMetric(collector.statusUsMgmt, prometheus.GaugeValue, 3)
					case "maintenance":
						ch <- prometheus.MustNewConstMetric(collector.statusUsMgmt, prometheus.GaugeValue, 4)
					}
				}
			}
		}
	}

}
